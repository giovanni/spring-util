package info.atende.webutil.spring.scope;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.web.context.request.FacesRequestAttributes;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * Proposito do Arquivo
 * User: giovanni
 * Date: 25/04/12
 * Time: 14:36
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class ViewScope implements Scope {
    public static final String VIEW_SCOPE_CALLBACKS = "viewScope.callbacks";
    public Object get(String name, ObjectFactory<?> objectFactory) {
        Object instance = getViewMap().get(name);
        if(instance == null) {
            Map viewMap = getViewMap();
            instance = objectFactory.getObject();
            synchronized (viewMap){
                viewMap.put(name,instance);
            }
        }
        return instance;
    }
    public Object remove(String name) {
        Object instance = getViewMap().remove(name);
        if(instance != null) {
            Map<String,Runnable> callbacks = (Map<String, Runnable>) getViewMap().get(VIEW_SCOPE_CALLBACKS);
            if(callbacks != null) {
                callbacks.remove(name);
            }
        }
        return instance;
    }
    public void registerDestructionCallback(String name, Runnable runnable) {
        Map<String,Runnable> callbacks = (Map<String, Runnable>) getViewMap().get(VIEW_SCOPE_CALLBACKS);
        if(callbacks != null) {
            callbacks.put(name,runnable);
        }
    }
    public Object resolveContextualObject(String name) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesRequestAttributes facesRequestAttributes = new FacesRequestAttributes(facesContext);
        return facesRequestAttributes.resolveReference(name);
    }
    public String getConversationId() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesRequestAttributes facesRequestAttributes = new FacesRequestAttributes(facesContext);
        return facesRequestAttributes.getSessionId() + "-" + facesContext.getViewRoot().getViewId();
    }
    private Map<String,Object> getViewMap() {
        return FacesContext.getCurrentInstance().getViewRoot().getViewMap();
    }
}
